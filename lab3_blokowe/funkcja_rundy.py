EXPANSION_TABLE = [32, 1, 2, 3, 4, 5, 4, 5, 6, 7, 8, 9, 8, 9, 10, 11, 12, 13, 12, 13, 14, 15, 16, 17,
                   16, 17, 18, 19, 20, 21, 20, 21, 22, 23, 24, 25, 24, 25, 26, 27, 28, 29, 28, 29, 30, 31, 32, 1]


def apply_Expansion(expansion_table, bits_32):
    """ This will take expansion table and 32-bit binary string as input and output a 48-bit binary stirng"""
    bits48 = ""
    for index in expansion_table:
        bits48 += bits_32[index - 1]
    return bits48


# test
# bits32 = '11110000101010101111000010101010'
# out_bits48 = apply_Expansion(EXPANSION_TABLE,bits32)
# print("Rozszerzony blok wiadomości: ", out_bits48)
# # 011110100001010101010101011110100001010101010101

def XOR(bits1, bits2):
    """perform a XOR operation and return the output"""
    # Assuming both bit string of same length
    xor_result = ""
    for index in range(len(bits1)):
        if bits1[index] == bits2[index]:
            xor_result += '0'
        else:
            xor_result += '1'
    return xor_result


# bits1 = '1100'
# bits2 = '1010'
# print(XOR(bits1, bits2))
# # output: '0110'

SBOX = [
    # Box-1
    [
        [14, 4, 13, 1, 2, 15, 11, 8, 3, 10, 6, 12, 5, 9, 0, 7],
        [0, 15, 7, 4, 14, 2, 13, 1, 10, 6, 12, 11, 9, 5, 3, 8],
        [4, 1, 14, 8, 13, 6, 2, 11, 15, 12, 9, 7, 3, 10, 5, 0],
        [15, 12, 8, 2, 4, 9, 1, 7, 5, 11, 3, 14, 10, 0, 6, 13]
    ],
    # Box-2

    [
        [15, 1, 8, 14, 6, 11, 3, 4, 9, 7, 2, 13, 12, 0, 5, 10],
        [3, 13, 4, 7, 15, 2, 8, 14, 12, 0, 1, 10, 6, 9, 11, 5],
        [0, 14, 7, 11, 10, 4, 13, 1, 5, 8, 12, 6, 9, 3, 2, 15],
        [13, 8, 10, 1, 3, 15, 4, 2, 11, 6, 7, 12, 0, 5, 14, 9]
    ],

    # Box-3

    [
        [10, 0, 9, 14, 6, 3, 15, 5, 1, 13, 12, 7, 11, 4, 2, 8],
        [13, 7, 0, 9, 3, 4, 6, 10, 2, 8, 5, 14, 12, 11, 15, 1],
        [13, 6, 4, 9, 8, 15, 3, 0, 11, 1, 2, 12, 5, 10, 14, 7],
        [1, 10, 13, 0, 6, 9, 8, 7, 4, 15, 14, 3, 11, 5, 2, 12]

    ],

    # Box-4
    [
        [7, 13, 14, 3, 0, 6, 9, 10, 1, 2, 8, 5, 11, 12, 4, 15],
        [13, 8, 11, 5, 6, 15, 0, 3, 4, 7, 2, 12, 1, 10, 14, 9],
        [10, 6, 9, 0, 12, 11, 7, 13, 15, 1, 3, 14, 5, 2, 8, 4],
        [3, 15, 0, 6, 10, 1, 13, 8, 9, 4, 5, 11, 12, 7, 2, 14]
    ],

    # Box-5
    [
        [2, 12, 4, 1, 7, 10, 11, 6, 8, 5, 3, 15, 13, 0, 14, 9],
        [14, 11, 2, 12, 4, 7, 13, 1, 5, 0, 15, 10, 3, 9, 8, 6],
        [4, 2, 1, 11, 10, 13, 7, 8, 15, 9, 12, 5, 6, 3, 0, 14],
        [11, 8, 12, 7, 1, 14, 2, 13, 6, 15, 0, 9, 10, 4, 5, 3]
    ],
    # Box-6

    [
        [12, 1, 10, 15, 9, 2, 6, 8, 0, 13, 3, 4, 14, 7, 5, 11],
        [10, 15, 4, 2, 7, 12, 9, 5, 6, 1, 13, 14, 0, 11, 3, 8],
        [9, 14, 15, 5, 2, 8, 12, 3, 7, 0, 4, 10, 1, 13, 11, 6],
        [4, 3, 2, 12, 9, 5, 15, 10, 11, 14, 1, 7, 6, 0, 8, 13]

    ],
    # Box-7
    [
        [4, 11, 2, 14, 15, 0, 8, 13, 3, 12, 9, 7, 5, 10, 6, 1],
        [13, 0, 11, 7, 4, 9, 1, 10, 14, 3, 5, 12, 2, 15, 8, 6],
        [1, 4, 11, 13, 12, 3, 7, 14, 10, 15, 6, 8, 0, 5, 9, 2],
        [6, 11, 13, 8, 1, 4, 10, 7, 9, 5, 0, 15, 14, 2, 3, 12]
    ],
    # Box-8

    [
        [13, 2, 8, 4, 6, 15, 11, 1, 10, 9, 3, 14, 5, 0, 12, 7],
        [1, 15, 13, 8, 10, 3, 7, 4, 12, 5, 6, 11, 0, 14, 9, 2],
        [7, 11, 4, 1, 9, 12, 14, 2, 0, 6, 10, 13, 15, 3, 5, 8],
        [2, 1, 14, 7, 4, 10, 8, 13, 15, 12, 9, 0, 3, 5, 6, 11]
    ]

]

import textwrap


# podział wiadomości na 6-bitowe porcje 48/6 = 8
def split_in_6bits(XOR_48bits):
    """split 48 bits into 6 bits each """
    list_of_6bits = textwrap.wrap(XOR_48bits, 6)
    return list_of_6bits


## Pomocnicze funkcje
def get_first_and_last_bit(bits6):
    """Return first and last bit from a binary string"""
    twobits = bits6[0] + bits6[-1]
    return twobits


def get_middle_four_bit(bits6):
    """Return first and last bit from a binary string"""
    fourbits = bits6[1:5]
    return fourbits


def binary_to_decimal(binarybits):
    """ Convert binary bits to decimal"""
    # helps during list access
    decimal = int(binarybits, 2)
    return decimal


def decimal_to_binary(decimal):
    """ Convert decimal to binary bits"""
    binary4bits = bin(decimal)[2:].zfill(4)
    return binary4bits


def sbox_lookup(sboxcount, first_last, middle4):
    """ take three parameter and access the Sbox accordingly and return the value"""
    d_first_last = binary_to_decimal(first_last)
    d_middle = binary_to_decimal(middle4)

    sbox_value = SBOX[sboxcount][d_first_last][d_middle]
    return decimal_to_binary(sbox_value)


# Test
# bits48 = '011110100001010101010101011110100001010101010101'
# sixbitslist = split_in_6bits(bits48)
# print("Blok podzielony na 6-bitowe porcje:", sixbitslist)
# bits6 = sixbitslist[0]
# first_last = get_first_and_last_bit(bits6)  # '10' -> 2
# print("Liczba z pierwszego i ostatniego bitu: ", first_last)
# middle4 = get_middle_four_bit(bits6)  # '0000' -> 0
# print("Liczba z środkowych 4 bitów:", middle4)
#
# sboxcount = 1
# result = sbox_lookup(sboxcount, first_last, middle4)
# print("4-bitowa wartość z s-box", result)

PERMUTATION_TABLE = [16, 7, 20, 21, 29, 12, 28, 17, 1, 15, 23, 26, 5, 18, 31, 10,
                     2, 8, 24, 14, 32, 27, 3, 9, 19, 13, 30, 6, 22, 11, 4, 25]


def apply_Permutation(permutation_table, sbox_32bits):
    """ It takes Sboxes output and a permutation table and return 32 bit binary string"""
    final_32bits = ""
    for index in permutation_table:
        final_32bits += sbox_32bits[index - 1]
    return final_32bits


def functionF(pre32bits, key48bits):
    """This is main function to perform function F """
    result = ''
    expanded_left_half = apply_Expansion(EXPANSION_TABLE, pre32bits)
    xor_value = XOR(expanded_left_half, key48bits)
    bits6list = split_in_6bits(xor_value)
    for sboxcount, bits6 in enumerate(bits6list):
        first_last= get_first_and_last_bit(bits6)
        middle4 = get_middle_four_bit(bits6)
        sboxvalue = sbox_lookup(sboxcount, first_last, middle4)
        result += sboxvalue

    final32bits = apply_Permutation(PERMUTATION_TABLE, result)
    return final32bits

#test
bits32 = '11110000101010101111000010101010'
key48bits = '110010110011110110001011000011100001011111110101'
print("Wynik aplikowania F(): ", functionF(bits32, key48bits))