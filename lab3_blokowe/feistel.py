import generowanie_kluczy
import  funkcja_rundy

def hexTobinary(hexdigits):
	binarydigits = ""
	for hexdigit in hexdigits:
		binarydigits += bin(int(hexdigit,16))[2:].zfill(4)
	return binarydigits



INITIAL_PERMUTATION_TABLE = ['58 ', '50 ', '42 ', '34 ', '26 ', '18 ', '10 ', '2',
			 '60 ', '52 ', '44 ', '36 ', '28 ', '20 ', '12 ', '4',
			 '62 ', '54 ', '46 ', '38 ', '30 ', '22 ', '14 ', '6',
			'64 ', '56 ', '48 ', '40 ', '32 ', '24 ', '16 ', '8',
			'57 ', '49 ', '41 ', '33 ', '25 ', '17 ', '9 ', '1',
			 '59 ', '51 ', '43 ', '35 ', '27 ', '19 ', '11 ', '3',
			 '61 ', '53 ', '45 ', '37 ', '29 ', '21 ', '13 ', '5',
			 '63 ', '55 ', '47 ', '39 ', '31 ', '23 ', '15 ', '7']

def apply_initial_p(P_TABLE, PLAINTEXT):
	permutated_M = ""
	for index in P_TABLE:
		permutated_M += PLAINTEXT[int(index)-1]
	return permutated_M

# #Test
# M = '0123456789ABCDEF'
# plaintext = hexTobinary(M)
# print("Binarna postać tekstu jawnego:", plaintext)
# print("Blok po początkowej permutacji:", apply_initial_p(INITIAL_PERMUTATION_TABLE,plaintext))
#
# ## Wyjście:
#
# # 0000000100100011010001010110011110001001101010111100110111101111
# # 1100110000000000110011001111111111110000101010101111000010101010
#
def spliHalf(binarybits):
    return binarybits[:32], binarybits[32:]


# M = '0123456789ABCDEF'
# p_plaintext = hexTobinary(M)
# L0, R0 = spliHalf(p_plaintext)
# print("Blok po podziale: ", L0, R0)

# Output
# '00000001001000110100010101100111' '10001001101010111100110111101111'
INVERSE_PERMUTATION_TABLE = ['40 ', '8 ', '48 ', '16 ', '56 ', '24 ', '64 ', '32',
			     '39 ', '7 ', '47 ', '15 ', '55 ', '23 ', '63 ', '31',
			     '38 ', '6 ', '46 ', '14 ',  '54 ', '22 ', '62 ', '30',
			     '37 ', '5 ', '45 ', '13 ', '53 ', '21 ', '61 ', '29',
			     '36 ', '4 ', '44 ', '12 ', '52 ', '20 ', '60 ', '28',
			     '35 ', '3 ', '43 ', '11 ', '51 ', '19 ', '59 ', '27',
			     '34 ', '2 ', '42 ', '10 ', '50 ', '18 ', '58 ', '26',
			     '33 ', '1 ', '41 ', '9 ', '49 ', '17 ', '57 ', '25']


def apply_inverse_p(INVERSE_P_TABLE, ROUND_OUT):
	cipher = ""
	for index in INVERSE_P_TABLE:
		cipher += ROUND_OUT[int(index)-1]
	return cipher


# # Test
# R16 = '11001100000000001100110011111111'
# L16 = '11110000101010101111000010101010'
#
# cipher = apply_initial_p(INVERSE_PERMUTATION_TABLE, R16+L16)
#
# print("Blok po finalnej permutacji:", cipher)
# Ouptput
# 0000000100100011010001010110011110001001101010111100110111101111 64

# 1. wygenerujmy klucze rundy
key_64bits = "0001001100110100010101110111100110011011101111001101111111110001"
roundkeys = generowanie_kluczy.generate_keys(key_64bits)
# Suppose initial R and L value is
R = '11001100000000001100110011111111'
L = '11110000101010101111000010101010'

print("Blok wejściowy", R+L)
# Sieć Feistela - uzupełnij kod wg schematu powyżej, użyj podklucza z dowolnej rundy


print("Blok wyjściowy", R+L)
# Wykonaj operacje odwrotne w sieci Feistela - sprawdź czy szyfrowanie się odwróci

import binascii

M = '0123456789ABCDEF'
key = '12345678ABCEDFF9'


# C = '9831DEFB82F48A97' #dla potrzeb weryfikacji poprawności

def DES_encrypt(message, key):
    cipher = ""
    plaintext_bits = hexTobinary(message)
    key_bits = hexTobinary(key)
    # zaimplementuj szyfrowanie DES zgodnie z diagramem w punkcie 1 powyżej.
    L0, R0 = spliHalf(key_bits)
    fun_f_out = funkcja_rundy.functionF(R0, key_bits)
    xored = funkcja_rundy.XOR(fun_f_out, L0)
    cipher = R0 +

    return cipher


print("Klucz:      ", hexTobinary(key))
print("Tekst jawny:", hexTobinary(M))
print("Szyfrogram: ", DES_encrypt(M, key))
print("Szyfrogram: ", hex(int(DES_encrypt(M, key), 2)))


#deszyfrowanie jest zadaniem obowiązkowym
#ZADANIE DODATKOWE: zrobić notebooka o innym szyfrze

#POD KARTKÓWECZKĘ:
#tryb algorytmu szyfrowania, co to jest sbox, czemu jest taka fajna