import math
import random


def trivial_find_period(x, N):
    n = 1
    t = x
    while t != 1:
        t *= x
        t %= N
        n += 1
    return n


# teścik:
# N=15
# for i in range(2, 20, 1):
#     if (math.gcd(i, N)==1):
#         print('Okres funkcji F(a)=x^a mod N dla x=', i, 'i N=', N, 'wynosi: ', trivial_find_period(i, N))

###### Test pierwszości Rabina-Millera


def rabinMiller(num):
    d = num - 1  ##obliczamy wartości d i sa
    s = 0
    while d % 2 == 0:  ##usuwamy z num-1 dzielniki 2 zliczając je w s
        d = d // 2
        s += 1

    for trials in range(20):  ## wykonujemy n testów R-B
        a = random.randrange(2, num - 1)  ##losujemy baze a
        b = pow(a, d, num)  ### pierwszy wyraz ciągu
        if (b != 1) and (b != num - 1):  ## jeśli b nie spełnia warunków losujemy innego świadka
            i = 1
            while (i < s) and (b != (num - 1)):
                b = (b ** 2) % num  ## obliczamy kolejne wyrazy ciągu R-M
                if (b == 1):  ## tylko ostatni wyraz może mieć wartość 1
                    return False
                i += 1

            if (b != num - 1):  ##przedpstatni wyraz musi mieć wartość num -1
                return False
                ### jeśli wykonaliśmy n testów i żaden nie zakończył się False
    return True


def isPrime(num):
    if (num <= 2):
        return False  # oczywista oczywistość
    return rabinMiller(num)


# napisz funkcję generującą liczbę pierwszą
def generatePrime(keysize):
    while True:
        num = random.randrange(2 ** (keysize - 1), 2 ** (keysize))
        if isPrime(num):
            return num


tp = generatePrime(16)
tq = generatePrime(16)

print('Liczby pierwsze:', tp, tq, ' i ich iloczyn', tp * tq)


def shors_algorithm_classical(N):
    x = random.randint(1, N)
    # Jeśli 𝑥 i 𝑁 mają wspólne czynniki pierwsze to gcd(𝑥,𝑁) będzie równie 𝑝 or 𝑞. STOP.
    if math.gcd(x, N) != 1:
        return x, 0, math.gcd(x, N), N / math.gcd(x, N)
    else:
        r = trivial_find_period(x, N)
        if r % 2 == 0:                  #r musi być parzyste żeby r // zadziałało
            print("r parzyste")
            p = math.gcd(pow(x, r // 2) + 1, N)     #pow realizuje szybkie potęgowanie modularne
            q = math.gcd(pow(x, r // 2) - 1, N)
            return x, r, p, q
        else:
            print("Znalezino nieparzyste r, powrót do początku")
            return shors_algorithm_classical(N)


x, r, p, q = shors_algorithm_classical(tp * tq)
print("Licza złożona N = ", tp * tq, ", względnie pierwsza z x = ", x, ", okres r = ", r, ", czynniki pierwsze = ", p,
      " and ", q, sep="")
