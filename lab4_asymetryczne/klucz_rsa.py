import random, sys, os, wstep


def mod_inverse_search(e, modulus):
    for i in range(modulus):
        if (e * i) % modulus == 1:
            return i


def mod_inverse_random_search(e, modulus):
    result = random.randint(1, modulus - 1)
    while (e * result) % modulus != 1:
        result = random.randint(1, modulus - 1)
    return result


def mod_inverse_extended_euklides(a, b):
    a_orig = a
    b_orig = b
    x = 1
    y = 0
    r = 0
    s = 1
    while b != 0:
        c = a % b
        q = a // b  # integer division
        a = b
        b = c
        r_prim = r
        s_prim = s
        r = x - q * r
        s = y - q * s
        x = r_prim
        y = s_prim

    return x % b_orig   #mod added to handle negative x


def generateKey(keySize):
    print('1. Generujemy liczby p i q')
    p = wstep.generatePrime(keySize)
    q = wstep.generatePrime(keySize)
    print(p, " ", q)

    print('2. Generujemy wykładnik publiczny (względnie pierwszy z (p-1)(q-1))')
    # np. poszukiwanie losowe
    e = wstep.publicExponent((p - 1) * (q - 1))
    print(e)

    print('3. Obliczamy wykładnik prywatny: odwrotność e modulo (p-1)(q-1)')
    # d = (1.0 / e) % ((p - 1) * (q - 1))
    # d = odwrotnosc_modulo(e, (p - 1) * (q - 1))
    d = mod_inverse_extended_euklides(e, (p - 1) * (q - 1))
    print(d)

    n = p * q
    print("n:")
    print(n)

    publicKey = (n, e)
    privateKey = (n, d)
    print('Klucz publiczny:', publicKey)
    print('Klucz prywatny:', privateKey)
    return (publicKey, privateKey)


def makeKeyFiles(keySize):
    public, private = generateKey(16)
    print(public, private)


print('Generujemy klucze publiczny i prywatny')
makeKeyFiles(8)


def encrypt(message, modulus, exp):
    # kod szyfrowania
    message_ascii = []
    for i in message:
        message_ascii.append(ord(i))
    print(message_ascii)

    message_encrypted = []
    for i in message_ascii:
        # message_encrypted.append((i**exp) % modulus) alternatywnie:
        message_encrypted.append(pow(i, exp, modulus))

    return message_encrypted


def decrypt(message_encrypted, modulus, exp):
    # kod deszyfrowania 𝑚=𝑐^𝑑 (𝑚𝑜𝑑 𝑛)
    message = []
    for i in message_encrypted:
        message.append((pow(i, exp)) % modulus)
    print("decrypted string:")
    print(message)
    message_str = ''
    for c in message:
        message_str += chr(c)

    return ''.join(message_str)


keyPublic, keyPrivate = generateKey(8)
print("klucz publiczny i prywatny:")
print(keyPublic, keyPrivate)

ciphertext = encrypt('asdasdasdasd', keyPublic[0], keyPublic[1])  # publicKey = (n, e)
print("tekst zaszyfrowany:")
print(ciphertext)
decrypted = decrypt(ciphertext, keyPrivate[0], keyPrivate[1])  # privateKey = (n, d)
print("z powrotem odszyfrowany:")
print(decrypted)
