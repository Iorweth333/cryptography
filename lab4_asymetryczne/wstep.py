import random


## Funkcje pomocnicze
def gcd(a, b):
    # GCD - Greatest Common Divisor, Największy wspólny dzielnik
    while a != 0:
        a, b = b % a, a
    return b


def findModInverse(a, m):
    # Zwraca liczbę x odwrotną do a ciele skończonym modulo m
    # czyli (a*x) % m =1

    if gcd(a, m) != 1:
        return None  # a i m muszą być względnie pierwsze aby istniał element odwrotny

    # Rozszerzony algorytm Euklidesa
    u1, u2, u3 = 1, 0, a
    v1, v2, v3 = 0, 1, m
    while v3 != 0:
        q = u3 // v3  # // operator dzielenie całkowitoliczbowego
        v1, v2, v3, u1, u2, u3 = (u1 - q * v1), (u2 - q * v2), (u3 - q * v3), v1, v2, v3
    return u1 % m


###### Test pierwszości Rabina-Millera
import random


def rabinMiller(num):
    d = num - 1  ##obliczamy wartości d i sa
    s = 0
    while d % 2 == 0:  ##usuwamy z num-1 dzielniki 2 zliczając je w s
        d = d // 2
        s += 1

    for trials in range(6):  ## wykonujemy n testów R-B
        a = random.randrange(2, num - 1)  ##losujemy baze a
        b = pow(a, d, num)  ### pierwszy wyraz ciągu
        if (b != 1) and (b != num - 1):  ## jeśli b nie spełnia warunków losujemy innego świadka
            i = 1
            while (i < s) and (b != (num - 1)):
                b = (b ** 2) % num  ## obliczamy kolejne wyrazy ciągu R-M
                if (b == 1):  ## tylko ostatni wyraz może mieć wartość 1
                    return False
                i += 1

            if (b != num - 1):  ##przedpstatni wyraz musi mieć wartość num -1
                return False
                ### jeśli wykonaliśmy n testów i żaden nie zakończył się False
    return True


def isPrime(num):
    if (num <= 2):
        return False  # oczywista oczywistość
    # opcjonalne można sprawdzić czy małe liczby pierwsze nie są czynnikami badanej liczby

    return rabinMiller(num)


# napisz funkcję generującą liczbę pierwszą

def generatePrime(key_size):
    is_prime = False
    while not is_prime:
        num = random.randint(2 ** (key_size - 1), 2 ** key_size)
        is_prime = isPrime(num)
    return num


# testedNumber = 655337
#
# if (isPrime(testedNumber)):
#     print(testedNumber, " jest liczbą pierwszą")
# else:
#     print(testedNumber, " nie jest liczbą pierwszą")

newprime = generatePrime(16)
print(isPrime(newprime))
print(newprime)


def nwd(a, b):
    while a != b:
        if a > b:
            a -= b
        else:
            b -= a
    return a


def relativelyFirst(a, b):
    if nwd(a, b) == 1:
        return True
    else:
        return False


def publicExponent(relatively_first_with):
    while True:
        num = random.randint(0, 1000000000)
        if relativelyFirst(num, relatively_first_with):
            return num
