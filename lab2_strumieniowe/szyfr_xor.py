import random
import os


def randomBytes(n):
    return bytes(random.getrandbits(8) for i in range(n))


key = 'Key'

message = 'This is a secret'
random.seed(key)

plaintext = list(message)
ciphertext = []
for c in plaintext:
    keyStream = int.from_bytes(randomBytes(1), byteorder='big')
    ciphertext.append(chr(ord(c) ^ keyStream))

encrypted_message = ''.join(ciphertext)
print("ciphertext: ")
print(ciphertext)
print("Szyfrogram: ")
print(encrypted_message)

random.seed(key)

plaintext.clear()
for c in ciphertext:
    keyStream = int.from_bytes(randomBytes(1), byteorder='big')
    plaintext.append(chr(ord(c) ^ keyStream))

decrypted_message = ''.join(plaintext)
print("odszyfrowana wiadomosc", decrypted_message)
