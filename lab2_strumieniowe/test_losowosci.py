import numpy
from scipy import special as spc


# 2.2 Frequency Test within a Block 2.2.1 Test Purpose The focus of the test is the proportion of ones within M-bit blocks.
# The purpose of this test is to determine whether the frequency of ones in an M-bit block is approximately M/2,
# as would be expected under an assumption of randomness.
# For block size M=1, this test degenerates to test 1, the Frequency (Monobit) test.

def blocks(data, block_size):
    """Yield successive n-sized chunks from l."""
    for i in range(0, len(data), block_size):
        yield data[i:i + block_size]


def proportion_of_ones(block):
    """Returns number of ones in the block divided by the length of the block"""
    sum = 0
    for i in block:
        sum += int(i)
    return sum / len(block)


def chi_square(proportions_arr, block_size):  # nie wiem, czemu taki wzór na chikwadrat, ale taki podali
    """calculates chi square for the purpose of this specific application"""
    sum = 0
    for p in proportions_arr:
        sum += (p - 0.5) * (p - 0.5)
    return 4 * block_size * sum


def BlockFrequency(data: str, block_size: int):
    """Main function of the test. If the computed P-value is < 0.01, then conclude that the sequence is non-random.
    Otherwise, conclude that the sequence is random. """
    proportions_arr = []

    for block in blocks(data, block_size):
        if len(block) == block_size:  # last block may be incomplete- it's discarded then
            proportions_arr.append(proportion_of_ones(block))
    chi_sq = chi_square(proportions_arr, block_size)
    blocks_num = len(proportions_arr)
    Pvalue = spc.gammaincc(blocks_num / 2, chi_sq / 2)
    return Pvalue


# Generowanie n-bitowego ciągu
n = 1024
block_size = 8

bitString = []
for i in range(0, n):
    x = str(numpy.random.randint(0, 2))
    bitString.append(x)
arr = ''.join(bitString)
print(arr)
print("Calculated Pvalue:")
Pval = BlockFrequency(arr, block_size)
print(Pval)

if Pval < 0.01:
    print("The sequence IS NOT random")
else:
    print("The sequence is random. Congratulations")
